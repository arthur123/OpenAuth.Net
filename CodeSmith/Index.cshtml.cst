﻿<%-- 
Name: Database Table Properties
Author: Paul Welter
Description: Create a list of properties from a database table
--%>
<%@ CodeTemplate Language="C#" Encoding="utf-8" TargetLanguage="C#" Debug="True" Description="Create a list of properties from database table." %>
<%@ Property Name="SourceTable" Type="SchemaExplorer.TableSchema" Category="Context" Description="Table that the object is based on." %>
<%@ Property Name="ModuleName" Type="String" Category="Context" Description="模块名称" %>
<%@ Map Name="CSharpAlias" Src="System-CSharpAlias" Description="System to C# Type Map" %>
<%@ Assembly Name="SchemaExplorer" %>
<%@ Import Namespace="SchemaExplorer" %>

@{
    string _prefix = "<%=ModuleName%>";
    var _treeId = _prefix + "Tree";
    var _gridId = _prefix + "Grid";
    var _treeDetail = _prefix + "Detail";
}
<div class="bjui-pageContent">
    <div class="clearfix">
        <div style="float: left; width: 220px; overflow: auto;" class="table table-bordered">
            <ul id="@_treeId" class="ztree"></ul>
        </div>

        <div id="@_treeDetail" style="margin-left: 225px;">
        </div>
    </div>
</div>


<script type="text/javascript">
    var gridid = '#@_gridId';
    var selectedId = 0;
    $(document).ready(function () {
        initZtree();
        loadDataGrid();
    });
    //加载数据到datagrid
    function loadDataGrid() {
        //b-jui的datagrid需要重新处理HTML
        $('#@_treeDetail').empty()
            .append('<table id="@_gridId" class="table table-bordered"></table>');

        $(gridid).datagrid({
            gridTitle: '列表',
            showToolbar: true,
            filterThead: false,
            toolbarItem: 'refresh, |, del',
            toolbarCustom: '<a href="/<%=ModuleName%>Manager/Add" class="btn btn-green" data-icon ="plus"  ' +
                'data-toggle="dialog" data-id="dialog-mask" data-mask="true" data-on-close="refreshGrid">添加</a>' +
                '<button class=" btn-green" onclick="edit<%=ModuleName%>()" data-icon="pencil" type="button">编辑</button>',
            columns: [
            <% foreach (ColumnSchema column in this.SourceTable.Columns) {  %>
               {
                    name: '<%=column.Name%>',
                    label: '<%=column.Description%>',
                     width: 100
                    <%if(column.IsPrimaryKeyMember){ %>
                    , hide: true
                    <%} %>
                    <%else if(CSharpAlias[column.SystemType.FullName] == "System.DateTime") {%>
                    , type: 'date',
                    pattern: 'yyyy-MM-dd HH:mm:ss'
                    <%} %>     
                    <%else if(CSharpAlias[column.SystemType.FullName] == "bool") {%>
                     ,type: 'select',
                    align: 'center',
                    items: [{ '0': '否' }, { '1': '是' }],
                    <%} %>  
                      <%else if(CSharpAlias[column.SystemType.FullName] == "int") {%>
                     ,type: 'select',
                    align: 'center',
                    items: [{ '0': '默认' }, { '1': '状态1' }],
                    <%} %>  
               },    
             <% } %>
            ],
            dataUrl: '<%=ModuleName%>Manager/Load?orgId=' + selectedId,
            delUrl: '<%=ModuleName%>/Delete',
            delPK: "Id",
            fullGrid: true,
            showLinenumber: true,
            showCheckboxcol: true,
            paging: true,
            filterMult: false,
            showTfoot: true,
            height: '700',
            delCallback: function (delResult) {
                if (delResult.statusCode == "200")
                    loadDataGrid();
                else {
                    $(this).alertmsg('warn', delResult.message);
                }
            }
        });
    }

    function zTreeOnClick(event, treeId, treeNode) {
        selectedId = treeNode.Id;
        loadDataGrid();
    }

    function initZtree() {
        var setting = {
            view: {selectedMulti: false},
            data: {
                key: {
                    name: 'Name',
                    title: 'Name'
                },
                simpleData: {
                    enable: true,
                    idKey: 'Id',
                    pIdKey: 'ParentId',
                    rootPId: 'null'
                }
            },
            callback: {onClick: zTreeOnClick}
        };
        $.getJSON('OrgManager/LoadOrg', function (json) {
            var zTreeObj = $.fn.zTree.init($('#@_treeId'), setting, json);
            zTreeObj.expandAll(true);
        });
    }

    //获取勾选的值
    //column:为从0开始的列标识
    function getSelected(column) {
        var selected = $(gridid).data('selectedTrs');
        if (selected == null || selected.length == 0) {
            $(this).alertmsg('warn', '至少选择一个对象', {
                displayMode: 'slide',
                title: '重要提示'
            });
            return null;
        }

        //todo：下面这段只能chrome有效
        var records = new Array();
        selected.each(function () {
            records[records.length] = this.children[column].innerText;
        });

        return records[0];
    }



    //自定义的编辑按钮
    function edit<%=ModuleName%>() {
        var selected = getSelected(2);
        if (selected == null) return;

        $(this).dialog({
            id: 'editDialog',
            url: '/<%=ModuleName%>Manager/Add?id=' + selected,
            title: '编辑',
            onClose:function() {
                refreshGrid();
            }
        });

    }

    function refreshGrid() {
        $('#@_gridId').datagrid('refresh');
       // loadDataGrid();
    }
    //@@ sourceURL=<%=ModuleName%>ManagerIndex.js
</script>
